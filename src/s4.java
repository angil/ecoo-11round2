import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class s4 {
	static P3D[] q = {new P3D(4, 4, 4), new P3D(2, 2, 2), new P3D(0, 4, 4),
			new P3D(2, 2, 2), new P3D(0, 0, 4), new P3D(2, 2, 2),
			new P3D(0, 0, 0)};
//	static boolean temp = false;
	
	public static void main(String[] args) throws IOException {
		Scanner in = new Scanner(new File("DATA41.txt"));
		
		for (int i = 0; i < 5; i++) {
//			if (i == 3)
//				temp = true;
			P3D k = new P3D(in.nextInt() - 1, in.nextInt() - 1, in.nextInt() - 1);
			P3D goal = new P3D(in.nextInt() - 1, in.nextInt() - 1, in.nextInt() - 1);
			System.out.printf("There are %d knight moves from %s to %s%n", dfs(k, 0, goal), k.add(1, 1, 1).toString(), goal.add(1, 1, 1).toString());
		}
		
		in.close();
	}
	
	private static int dfs(P3D k, int n, P3D goal) {
		if (n > 6)
			return Integer.MIN_VALUE;
		if (k.oob())
			return Integer.MIN_VALUE;
		if (k.equals(goal))
			return 0;
//		if (temp && n == 1){
//			System.err.println(k + " " + q[n] + " " + n);
//			System.err.println(k.is(q[n]));
//		}
		if (n != 0 && k.is(q[n]))
			return Integer.MIN_VALUE;
		
		
		return 1 + min(dfs(k.add(0, 1, 2), n + 1, goal), dfs(k.add(0, 2, 1), n + 1, goal), dfs(k.add(1, 0, 2), n + 1, goal), dfs(k.add(1, 2, 0), n + 1, goal), dfs(k.add(2, 1, 0), n + 1, goal), dfs(k.add(2, 0, 1), n + 1, goal),

		dfs(k.add(0, 1, -2), n + 1, goal), dfs(k.add(0, 2, -1), n + 1, goal), dfs(k.add(1, 0, -2), n + 1, goal), dfs(k.add(1, -2, 0), n + 1, goal), dfs(k.add(2, -1, 0), n + 1, goal), dfs(k.add(2, 0, -1), n + 1, goal),

		dfs(k.add(0, -1, 2), n + 1, goal), dfs(k.add(0, -2, 1), n + 1, goal), dfs(k.add(-1, 0, 2), n + 1, goal), dfs(k.add(-1, 2, 0), n + 1, goal), dfs(k.add(-2, 1, 0), n + 1, goal), dfs(k.add(-2, 0, 1), n + 1, goal),

		dfs(k.add(0, -1, -2), n + 1, goal), dfs(k.add(0, -2, -1), n + 1, goal), dfs(k.add(-1, 0, -2), n + 1, goal), dfs(k.add(-1, -2, 0), n + 1, goal), dfs(k.add(-2, -1, 0), n + 1, goal), dfs(k.add(-2, 0, -1), n + 1, goal));
	}
	
	private static int min(int... a) {
		int min = Integer.MAX_VALUE;
		for (int w = 0; w < a.length; w++) {
			if (a[w] < min && a[w] >= 0)
				min = a[w];
		}
		return min;
	}
	
	private static class P3D {
		public int x, y, z;
		public P3D(int nx, int ny, int nz) {
			x = nx;
			y = ny;
			z = nz;
		}
		
		public boolean oob() {
			return x < 0 || y < 0 || z < 0 || x >= 5 || y >= 5 || z >= 5;
		}
		
		public boolean is(P3D p) {
			int dx, dy, dz;
			dx = Math.abs(x - p.x);
			dy = Math.abs(y - p.y);
			dz = Math.abs(z - p.z);
			
//			if (this.equals(new P3D(1,1,3)))
//				System.err.println(dx + " " + dy + " " + dz);
			
			if (dx == dy && dy == dz) // main diag
				return true;
			if ((dx == 0 && dy == dz) || (dz == 0 && dy == dx)
					|| (dy == 0 && dx == dz)) // plane diag
				return true;
			if ((dx == 0 && dy == 0) || (dz == 0 && dy == 0)
					|| (dx == 0 && dz == 0)) // plane line
				return true;
			
			return false;
		}
		
		public P3D add(int nx, int ny, int nz) {
			return new P3D(x + nx, y + ny, z + nz);
		}
		
		public boolean equals(P3D g) {
			return g.x == x && g.y == y && g.z == z;
		}
		
		public String toString() {
			return String.format("(%d,%d,%d)", x, y, z);
		}
	}
}
