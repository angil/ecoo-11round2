import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class s3 {
	public static void main(String[] args) throws IOException {
		Scanner in = new Scanner(new File("DATA31.txt"));
		
		int acc, index;			
		String[]prgm = new String[100];
		int[]data = new int[100];
		
		
		for (int c = 0; c < 5; c++) {
			acc = 0; index = 0;
			for (int i = 0; i < 100; i ++){
				prgm[i] = "00";
				data[i] = 0;
				
			}
			String s = in.nextLine();
			for (int g = 0; g < s.length()/2; g++) {
				prgm[g] = s.substring(g*2, g*2+2);
			}
			for (int i = 0; i < 100; i++) {
				if (prgm[i].equals("01")){
					acc = Integer.parseInt(prgm[i+1]);
					if (acc >= 100){
						acc -= 100;
					} else if (acc < 0){
						acc += 100;
					}
					i++;
				} else if (prgm[i].equals("11")){
					acc = data[Integer.parseInt(prgm[i+1])];
					if (acc >= 100){
						acc -= 100;
					} else if (acc < 0){
						acc += 100;
					}
					i++;
				} else if (prgm[i].equals("21")){
					acc = data[index];
					if (acc >= 100){
						acc -= 100;
					} else if (acc < 0){
						acc += 100;
					}
				} else if (prgm[i].equals("02")){
					acc += Integer.parseInt(prgm[i+1]);
					if (acc >= 100){
						acc -= 100;
					} else if (acc < 0){
						acc += 100;
					}
					i++;
				} else if (prgm[i].equals("12")){
					acc += data[Integer.parseInt(prgm[i+1])];
					if (acc >= 100){
						acc -= 100;
					} else if (acc < 0){
						acc += 100;
					}
					i++;
				} else if (prgm[i].equals("22")){
					acc += data[index];
					if (acc >= 100){
						acc -= 100;
					} else if (acc < 0){
						acc += 100;
					}
				} else if (prgm[i].equals("13")){
					data[Integer.parseInt(prgm[i+1])] = acc;
					if (data[Integer.parseInt(prgm[i+1])] >= 100){
						data[Integer.parseInt(prgm[i+1])] -= 100;
					} else if (data[Integer.parseInt(prgm[i+1])] < 0){
						data[Integer.parseInt(prgm[i+1])] += 100;
					}
					i++;
				} else if (prgm[i].equals("23")){
					data[index] = acc;
					if (data[index] >= 100){
						data[index] -= 100;
					} else if (data[index] < 0){
						data[index] += 100;
					}
				} else if (prgm[i].equals("31")){
					index = acc;
					if (index >= 100){
						index -= 100;
					} else if (index < 0){
						index += 100;
					}
				} else if (prgm[i].equals("32")){
					index--;
					if (index >= 100){
						index -= 100;
					} else if (index < 0){
						index += 100;
					}
				} else if (prgm[i].equals("33")){
					if (index > 0){
						i = Integer.parseInt(prgm[i+1]) - 1;
					} else {
						i++;
					}
				} else if (prgm[i].equals("40")){
					System.out.print(acc + " ");
				} else if (prgm[i].equals("99")){
					i = 100;
				}
			}
			System.out.println();
		}
		in.close();
	}
}
