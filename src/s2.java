import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class s2 {
	public static void main(String[] args) throws IOException {
		Scanner in = new Scanner(new File("DATA21.txt"));
		
		String o = "";
		int no = 0;
		char[][] orig = new char[6][6];
		int[][] orig1 = new int[6][6];
		int[][] orig2 = new int[6][6];
		int[][] orig3 = new int[6][6];
		int[][] orig4 = new int[6][6];
		for (int i = 0; i < 6; i++) {
			String temp = in.nextLine();
			orig[i] = temp.toCharArray();
			o = o + temp;
		}
		for (int i = 0; i < o.length(); i++)
			if (o.charAt(i) == 'o')
				no++;
		int[] s1 = new int[no];
		int[] s2 = new int[no];
		int[] s3 = new int[no];
		int[] s4 = new int[no];
		// first
		int n = 0;
		int count = 1;
		for (int j = 0; j < 6; j++) {
			for (int i = 0; i < 6; i++) {
				if (orig[i][j] == 'o') {
					orig1[i][j] = count;
					count = count + 1;
				} else {
					orig1[i][j] = 0;
				}
			}
		}
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 6; j++) {
				if (orig1[i][j] != 0) {
					s1[n++] = orig1[i][j];
				}
			}
		}
		// second
		n = 0;
		count = 1;
		for (int i = 5; i > -1; i--) {
			for (int j = 0; j < 6; j++) {
				if (orig[i][j] == 'o') {
					orig2[i][j] = count;
					count = count + 1;
				} else {
					orig2[i][j] = 0;
				}
			}
		}
		for (int j = 0; j < 6; j++) {
			for (int i = 5; i > -1; i--) {
				if (orig2[i][j] != 0) {
					s2[n++] = orig2[i][j];
				}
			}
		}
		// third
		n = 0;
		count = 1;
		for (int j = 5; j > -1; j--) {
			for (int i = 5; i > -1; i--) {
				if (orig[i][j] == 'o') {
					orig3[i][j] = count;
					count = count + 1;
				} else {
					orig3[i][j] = 0;
				}
			}
		}
		for (int i = 5; i > -1; i--) {
			for (int j = 5; j > -1; j--) {
				if (orig3[i][j] != 0) {
					s3[n++] = orig3[i][j];
				}
			}
		}
		// fourth
		n = 0;
		count = 1;
		for (int i = 0; i < 6; i++) {
			for (int j = 5; j > -1; j--) {
				if (orig[i][j] == 'o') {
					orig4[i][j] = count;
					count = count + 1;
				} else {
					orig4[i][j] = 0;
				}
			}
		}
		for (int j = 5; j > -1; j--) {
			for (int i = 0; i < 6; i++) {
				if (orig4[i][j] != 0) {
					s4[n++] = orig4[i][j];
				}
			}
		}
		
		n = 0;
		for (int i = 0; i < 5; i++) {
			String m = in.nextLine();
			String or = "";
			for (n = 0; n < m.length(); n++) {
				if ((n % (4 * no)) < no)
					or = or + m.charAt(s1[n % no] + n/no*no - 1);
				else if ((n % (4 * no)) < (2 * no))
					or = or + m.charAt(s2[n % no] + n/no*no - 1);
				else if ((n % (4 * no)) < (3 * no))
					or = or + m.charAt(s3[n % no] + n/no*no - 1);
				else if ((n % (4 * no)) < (4 * no))
					or = or + m.charAt(s4[n % no] + n/no*no - 1);
			}
			System.out.println(or);
		}
		
		in.close();
	}
}
