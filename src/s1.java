import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class s1 {
	public static void main(String[] args) throws IOException {
		Scanner in = new Scanner(new File("DATA11.txt"));
		for (int c = 0; c < 5; c++) {
			
			String next = in.nextLine();
			double n = Integer.parseInt(next.substring(0, next.indexOf('/')));
			double d = Integer.parseInt(next.substring(next.indexOf('/') + 1));
			String a = "" + (int)n;
			String b = "" + (int)d;
			String x = "", y = "";
			
			System.out.printf("%d/%d", (int)n, (int)d);
			
			int count = 0;
			while (count < 2) {
				for (int i = 0; i < a.length(); i++) {
					if (b.indexOf(a.charAt(i)) >= 0) {
						x = a.substring(0, i) + a.substring(i + 1);
						y = b.substring(0, b.indexOf(a.charAt(i)))
								+ b.substring(b.indexOf(a.charAt(i)) + 1);
						
					}
				}
				if(x.length() == 0 || y.length() == 0)
					break;
				double nn = Double.parseDouble(x);
				double nd = Double.parseDouble(y);
				
				if (n / d == nn / nd && n != nn && d != nd) {
					System.out.printf(" = %d/%d", (int) nn, (int) nd);
				} else if(count == 0) {
					System.out.printf(" cannot be simplified by the bogus method");
					break;
				}
				a = x;
				b = y;
				x = "";
				y = "";
				count ++;
			}
			System.out.println();
		}
		in.close();
	}
}